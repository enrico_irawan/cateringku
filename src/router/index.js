import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Home, SplashScreen, Login, Register, Cart, Account, Profile } from '../screens';
import { BottomNavigator } from '../components';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const MainApp = () => (
    <Tab.Navigator tabBar={props => <BottomNavigator {...props} />}>
        <Tab.Screen name="Home" component={Home} />
        <Tab.Screen name="Cart" component={Cart} />
        <Tab.Screen name="Account" component={Account} />
    </Tab.Navigator>
)

const Router = () => (
    <Stack.Navigator initialRouteName="Main">
        <Stack.Screen
            name="Splash"
            component={SplashScreen}
            options={{headerShown: false}}
        />
        <Stack.Screen
            name="Login"
            component={Login}
            options={{ headerShown: false }}
        />
        <Stack.Screen
            name="Register"
            component={Register}
            options={{ headerShown: false }}
        />
        <Stack.Screen
            name="Main"
            component={MainApp}
            options={{ headerShown: false }}
        />
        <Stack.Screen
            name="Profile"
            component={Profile}
            options={{ headerShown: false }}
        />
    </Stack.Navigator>
)

export default Router;