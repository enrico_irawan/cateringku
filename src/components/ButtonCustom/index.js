import React from 'react'
import { StyleSheet, Text, TouchableOpacity } from 'react-native'

export default function ButtonCustom({title, onPress, type}) {
    return (
       <TouchableOpacity onPress={onPress} style={styles.button(type)}>
           <Text style={styles.buttonText}>{title}</Text>
       </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    button: (type) =>({
        width: 232,
        height: 55,
        borderRadius: 6,
        backgroundColor: type === 'cancel' ? '#FF2E00' : '#FFA800',
        marginTop: 18,
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 4
    }),
    buttonText: {
        color: 'white',
    }
})
