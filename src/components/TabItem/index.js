import React from 'react'
import { StyleSheet, TouchableOpacity } from 'react-native'
import { IconHomeActive, IconHome, IconCartActive, IconCart, IconAccount, IconAccountActive } from '../../assets';

export default function TabItem({ title, active, onPress, onLongPress }) {
    const Icon = () => {
        if (title === 'Home') {
            return active ? <IconHomeActive /> : <IconHome />;
        }
        if (title === 'Cart') {
            return active ? <IconCartActive /> : <IconCart />;
        }
        if (title === 'Account') {
            return active ? <IconAccountActive /> : <IconAccount />;
        }
        return <IconDoctor />;
    };

    return (
        <TouchableOpacity
            style={styles.container}
            onPress={onPress}
            onLongPress={onLongPress}>
            <Icon style={styles.icon} />
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container: { alignItems: 'center' },
})
