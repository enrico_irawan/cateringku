import React, { useState } from 'react'
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native'
import { useDispatch, useSelector } from 'react-redux';
import { showMessage } from 'react-native-flash-message';

export default function Food({data, disable}) {
    const dispatch = useDispatch();
    const stateGlobal = useSelector(state => state);
    const disableGlobal = stateGlobal.disable;

    const onOrderPress = () => {
        dispatch({
            type: 'ORDER',
            value: {
                food: data.title,
                price: data.price,
                image: data.image,
            }
        })
        showMessage({
            message: "Yeay, Orderan anda sudah masuk ke keranjang",
            type: "success",
            duration: 2500
        });
    }

    return (
        <View style={styles.menuContent}>
            <View style={styles.menuCard}>
                <Image source={{uri: data.image}} style={styles.foodImages} />
                <View style={styles.subMenu}>
                    <View style={styles.foodText}>
                        <Text style={styles.foodTitle}>{data.title}</Text>
                        <Text style={styles.foodPrice}>Rp. {data.price}</Text>
                    </View>
                    <View style={styles.buttonAction}>
                        {/* <TouchableOpacity onPress={() => onPress('decrement')}>
                            <ButtonMinus />
                        </TouchableOpacity>
                        <Gap width={10} />
                        <Text>{orders.amount}</Text>
                        <Gap width={10} />
                        <TouchableOpacity onPress={() => onPress('increment')}>
                            <ButtonPlus />
                        </TouchableOpacity> */}
                        <TouchableOpacity disabled={disableGlobal} style={styles.orderButton(disableGlobal)} 
                         onPress={onOrderPress}>
                            <Text style={styles.buttonText}>Order</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    menuContent: {
        marginBottom: 10,
    },
    menuCard: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    foodImages: {
        width: 110,
        height: 77,
        borderRadius: 6
    },
    foodTitle: {
        fontFamily: 'Roboto-Light',
        fontSize: 16,
        maxWidth: '70%',
        fontWeight: 'bold'
    },
    foodPrice: {
        fontFamily: 'Roboto-Normal',
    },
    subMenu: {
        flexDirection: 'row',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    foodText: {
        marginLeft: 10
    },
    buttonAction: {
        flexDirection: 'row',
    },
    orderButton: (disable) => ({
        width: 70,
        height: 35,
        borderRadius: 6,
        backgroundColor: disable ? 'grey' : '#FFA800',
        justifyContent: 'center',
        alignItems: 'center',
        elevation: 6
    }),
    buttonText: {
        color: 'white',
        justifyContent: 'center'
    },
})
