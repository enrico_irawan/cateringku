import React from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';

export default function Order({data}) {
    // const [amount, setAmount] = useState(0);
    // const dispatch = useDispatch();

    // const onPress = (type) => {
    //     if(type === 'increment' && amount >=0){
    //         setAmount(amount + 1);
    //         dispatch({
    //             type: 'ADD',
    //             value: amount * data.price
    //         })
    //     }else if(type === 'decrement' && amount > 0){
    //         setAmount(amount - 1);
    //         dispatch({
    //             type: 'ADD',
    //             value: amount * data.price
    //         })
    //     }
    // }

    return (
        <View style={styles.menuContent}>
            <View style={styles.menuCard}>
                <Image source={{uri: data.image}} style={styles.foodImage} />
                <View style={styles.subMenu}>
                    <View style={styles.foodText}>
                        <View>
                            <Text style={styles.foodTitle}>{data.food}</Text>
                            <Text style={styles.foodPrice}>Rp. {data.price}</Text>
                        </View>
                        {/* <View style={styles.amount}>
                            <Text style={styles.amountText}>1 Porsi</Text>
                        </View> */}
                        {/* <View style={{justifyContent: 'space-between', flexDirection: 'row'}}>
                            <TouchableOpacity onPress={() => onPress('decrement')}>
                                <ButtonMinus />
                            </TouchableOpacity>
                            <Gap width={10} />
                            <Text>{amount}</Text>
                            <Gap width={10} />
                            <TouchableOpacity onPress={() => onPress('increment')}>
                                <ButtonPlus />
                            </TouchableOpacity>
                        </View> */}
                    </View>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    menuContent: {
        marginBottom: 10,
    },
    menuCard: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    foodTitle: {
        fontFamily: 'Roboto-Bold',
        fontSize: 12,
        maxWidth: '70%',
    },
    foodPrice: {
        fontFamily: 'Roboto-Normal'
    },
    subMenu: {
        flexDirection: 'row',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    foodText: {
        marginLeft: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        flex: 1
    },
    foodImage: {
        width: 110,
        height: 77,
        borderRadius: 6
    },
})
