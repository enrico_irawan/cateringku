import Logo from './CateringKu.svg';
import MadeBy from './MadeBy.svg';
import IconHomeActive from './IcHomeActive.svg';
import IconCartActive from './IcCartActive.svg';
import IconAccountActive from './IcAccountActive.svg';
import IconHome from './IcHome.svg';
import IconCart from './IcCart.svg';
import IconAccount from './IcAccount.svg';
import ButtonPlus from './plusButton.svg';
import ButtonMinus from './minusButton.svg';
import SuccessCheckout from './Success_checkout.svg';
import EmptyCart from './empty_illust.svg';
import IconInfo from './icInfo.svg';
import IconSignOut from './IcSignOut.svg';
import IconProfile from './IcAccountCirlce.svg';
import IconBack from './IcBack.svg';

export {
    Logo, 
    MadeBy, 
    IconHome, 
    IconHomeActive, 
    IconAccount, 
    IconAccountActive, 
    IconCart, 
    IconCartActive,
    ButtonPlus,
    ButtonMinus,
    SuccessCheckout,
    EmptyCart,
    IconInfo,
    IconSignOut,
    IconProfile,
    IconBack
};