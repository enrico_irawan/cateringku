import firebase from 'firebase';

firebase.initializeApp(
    {
        apiKey: "AIzaSyAR4PGmKYdKJSwa421CwPE7C-7kzDwq6l0",
        authDomain: "cateringku-8a4f2.firebaseapp.com",
        databaseURL: "https://cateringku-8a4f2.firebaseio.com",
        projectId: "cateringku-8a4f2",
        storageBucket: "cateringku-8a4f2.appspot.com",
        messagingSenderId: "392705228622",
        appId: "1:392705228622:web:2a2cbbd8c317932e77e81f"
    }
);

const Fire = firebase;
export default Fire;