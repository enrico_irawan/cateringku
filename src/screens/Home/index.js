import React, { useState, useEffect } from 'react'
import { StyleSheet, Text, View, Image, ImageBackground } from 'react-native'
import { ScrollView, TouchableOpacity, FlatList } from 'react-native-gesture-handler'
import { Gap, Food } from '../../components';
import {Fire} from '../../config'
import { useDispatch, useSelector } from 'react-redux';

export default function Home() {
    const [foods, setFoods] = useState([]);

    useEffect(() => {
        Fire.database()
            .ref('foods/')
            .once('value')
            .then(res => {
                if(res.val()){
                    setFoods(res.val());
                }
            } )
            .catch(e => e.message);
    }, [])

    return (
        <View style={styles.screen}>
            <View style={styles.header}>
                <View>
                    <Text style={styles.headerText}>Selamat Datang</Text>
                    <Text style={styles.name}>Enrico Irawan</Text>
                </View>
                <Image source={require('../../assets/images/PhotoProfileNull.png')} style={styles.photoProfile} />
            </View>
            <Gap height={31} />
            <View>
                <View>
                    <Text style={{ color: '#9D9898'}}>Sedia jasa catering, pilih paket nya sekarang</Text>
                </View>
                <Gap height={10} />
                <View style={styles.wrapperScroll}>
                        <ScrollView horizontal showsHorizontalScrollIndicator={false} contentContainerStyle={{paddingHorizontal: 24}}>
                            <View style={styles.card}>
                                <TouchableOpacity>
                                    <ImageBackground source={require('../../assets/images/UlangTahun.png')} style={styles.imageCategory}>
                                        <Text style={styles.imageText}>Ulang Tahun</Text>
                                    </ImageBackground>
                                </TouchableOpacity>
                            </View>
                            <Gap width={10} />
                            <View style={styles.card}>
                                <TouchableOpacity>
                                    <ImageBackground source={require('../../assets/images/Prasmanan.png')} style={styles.imageCategory}>
                                        <Text style={styles.imageText}>Prasmanan</Text>
                                    </ImageBackground>
                                </TouchableOpacity>
                            </View>
                            <Gap width={10} />
                            <View style={styles.card}>
                                <TouchableOpacity>
                                    <ImageBackground source={require('../../assets/images/Pernikahan.png')} style={styles.imageCategory}>
                                        <Text style={styles.imageText}>Pernikahan</Text>
                                    </ImageBackground>
                                </TouchableOpacity>
                            </View>
                        </ScrollView>
                </View>
            </View>
            <Gap height={25} />
            <View style={styles.menu}>
                <View style={styles.menuHeader}>
                    <Text style={{ color: '#9D9898', marginBottom: 10}}>Pilih Makanan</Text>
                    {/* <TouchableOpacity style={styles.orderButton} onPress={onOrderPress}>
                        <Text style={styles.buttonText}>Order</Text>
                    </TouchableOpacity> */}
                </View>
                <View>
                    <FlatList
                        data={foods}
                        showsVerticalScrollIndicator={false}
                        keyExtractor={item => {
                            if(item !== item){
                                return item.id
                            }
                        }}
                        renderItem={({item}) => {
                            if(item){
                                return <Food data={item} />
                            }
                        } }
                    />
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        paddingHorizontal: 24,
        paddingBottom: 340
    },
    header: {
        marginTop: 51,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    headerText: {
        fontFamily: 'Roboto-Medium',
        fontSize: 20,
        lineHeight: 23,
    },
    name: {
        fontFamily: 'Roboto-Medium',
        fontSize: 20,
        color: '#FFA800',
    },
    photoProfile: {
        width: 60,
        height: 60,
        alignSelf: 'flex-end'
    },
    card: {
        width: 120,
        height: 70,
        borderRadius: 10,
        overflow: 'hidden',
    },
    imageCategory: {
        width: '100%',
        height: '100%',
        borderRadius: 10,
        flexDirection: 'row',
        justifyContent: 'center'
    },
    imageText: {
        textAlign: 'center',
        alignSelf: 'flex-end',
        fontFamily: 'Roboto-Black',
        color: 'white'
    },
    wrapperScroll: {
        marginHorizontal: -24
    },
    menu: {

    },
    menuHeader: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    // orderButton: {
    //     width: 70,
    //     height: 35,
    //     borderRadius: 6,
    //     backgroundColor: '#FFA800',
    //     justifyContent: 'center',
    //     alignItems: 'center',
    //     elevation: 6
    // },
    // buttonText: {
    //     color: 'white',
    //     justifyContent: 'center'
    // },
})
