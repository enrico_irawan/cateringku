import React from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'
import { IconProfile, IconInfo, IconSignOut } from '../../assets'
import { TouchableOpacity } from 'react-native-gesture-handler'

export default function Account({navigation}) {
    return (
        <View style={styles.screen}>
            <Image source={require('../../assets/images/PhotoProfileNull.png')} style={styles.imageProfile} />
            <View style={styles.ruler} />
            <TouchableOpacity style={styles.menuContainer} onPress={() => navigation.navigate('Profile')}>
                <IconProfile />
                <Text style={styles.textMenu}>Profile</Text>
            </TouchableOpacity>
            <View style={styles.ruler} />
            <TouchableOpacity style={styles.menuContainer}>
                <IconInfo />
                <Text style={styles.textMenu}>About This App</Text>
            </TouchableOpacity>
            <View style={styles.ruler} />
            <TouchableOpacity style={styles.menuContainer}>
                <IconSignOut />
                <Text style={styles.textMenu}>Sign Out</Text>
            </TouchableOpacity>
            <View style={styles.ruler} />
        </View>
    )
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        justifyContent: 'center',
        padding: 24
    },
    imageProfile: {
        width: 100,
        height: 100,
        marginBottom: 51,
        alignSelf: 'center'
    },
    ruler: {
        borderWidth: 1,
        borderColor: '#9F8888',
        width: '100%'
    },
    menuContainer: {
        flexDirection: 'row',
        marginVertical: 18,
        alignItems: 'center',
    },
    textMenu: {
        fontSize: 18,
        marginLeft: 10
    }
})
