import React from 'react'
import { StyleSheet, Text, View, Image, TextInput } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { IconBack } from '../../assets'
import { ButtonCustom } from '../../components'

export default function Profile({navigation}) {
    return (
        <View style={styles.screen}>
            <TouchableOpacity onPress={() => navigation.pop()}>
                <IconBack />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => {}}>
                <Image source={require('../../assets/images/PhotoProfileNull.png')} style={styles.imageProfile} />
            </TouchableOpacity>
            <View style={styles.form}>
                <View>
                    <Text style={styles.label}>Username</Text>
                    <TextInput style={styles.textInput} />
                </View>
                <View>
                    <Text style={styles.label}>Alamat</Text>
                    <TextInput placeholder="Jln." style={styles.textInput} />
                </View>
                <View>
                    <Text style={styles.label}>Email</Text>
                    <TextInput placeholder="user@gmail.com" keyboardType="email-address" style={styles.textInput} />
                </View>
                <View>
                    <Text style={styles.label}>Password</Text>
                    <TextInput secureTextEntry style={styles.textInput} />
                </View>
                <ButtonCustom title="Simpan" onPress={() => { }} />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    screen: {
        padding: 24,
        flex: 1,
    },
    imageProfile: {
        alignSelf: 'center'
    },
    form: {
        alignItems: 'center'
    },
    label: {
        fontFamily: 'Roboto',
        marginBottom: 2,
        marginTop: 15
    },
    textInput: {
        width: 252,
        height: 60,
        borderWidth: 2,
        borderColor: '#FFA800',
        borderRadius: 8,
        fontSize: 20,
        padding: 10
    },
    button: {
        width: 232,
        height: 55,
        borderRadius: 6,
        backgroundColor: '#FFA800',
        marginTop: 26,
        alignItems: 'center',
        justifyContent: 'center',
    },
})
