import SplashScreen from './SplashScreen';
import Register from './Register';
import Login from './Login';
import Home from './Home'
import Cart from './Cart';
import Account from './Account';
import Profile from './Profile'

export {SplashScreen, Register, Login, Home, Cart, Account, Profile};