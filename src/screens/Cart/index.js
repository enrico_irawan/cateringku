import React, { useState } from 'react'
import { StyleSheet, Text, View, ScrollView } from 'react-native'
import { Order, ButtonCustom } from '../../components'
import { EmptyCart, SuccessCheckout } from '../../assets'
import { FlatList } from 'react-native-gesture-handler'
import { useSelector, useDispatch } from 'react-redux'
import AwesomeAlert from 'react-native-awesome-alerts';

export default function Cart() {
    const globalState = useSelector(state => state);
    const orders = globalState.orders;
    const dispatch = useDispatch();

    let total = orders.reduce((acc, curr) => {
        return acc + curr.price
    }, 0)

    const CartEmpty = () => (
        <View style={{flex: 1, padding: 24, justifyContent: 'space-between'}}>
            <Text style={styles.headerText}>Isi Keranjang Mu</Text>
            <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}>
                <EmptyCart />
                <Text style={{ textAlign: 'center', marginTop: 8, color: '#FFA800', fontSize: 20 }}>Kerajang mu masih kosong nih...</Text>
            </View>
        </View>
    )

    const Success = () => (
        <View style={{ flex: 1, padding: 24 }}>
            <Text style={styles.headerText}>Isi Keranjang Mu</Text>
            <View style={{alignItems: 'center', justifyContent: 'center', flex: 1}}>
                <SuccessCheckout />
                <Text style={{ textAlign: 'center', marginTop: 8, color: '#FFA800', fontSize: 20 }}>
                    Terima Kasih Sudah Memesan
                    Hope you enjoy it!
            </Text>
            </View>
        </View>
    )

    const onCancelPress = () => {
        dispatch({
            type: 'CANCEL',
        })
    }

    const onCheckoutPress = () => {
        alert('Pesanan Mu Telah Berhasil')
        dispatch({
            type: 'CANCEL',
        })
    }

    const CartContent = () => (
        <>
        <View style={styles.screen}>
            <Text style={styles.headerText}>Isi Keranjang Mu</Text>
            <View style={styles.wrapperVerticalScroll}>
               <FlatList 
                data={orders}
                keyExtractor={item => item.food}
                renderItem={({item}) => (
                    <Order data={item} />
                )}
               />
            </View>
            <View style={styles.ruler} />
            <View style={styles.price}>
                <Text style={styles.totalText}>TOTAL</Text>
                <Text style={styles.priceNumber}>Rp. {total}</Text>
            </View>
            <View style={styles.buttonContainer}>
                <ScrollView>
                    <ButtonCustom title="CHECKOUT" onPress={onCheckoutPress} />
                    <ButtonCustom title="BATALKAN PESANAN" type="cancel" onPress={onCancelPress} />
                </ScrollView>
            </View>
        </View>
        </>
    )

    if(orders.length === 0){
        return <CartEmpty />
    }else{
        return <CartContent />
    }
}

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        padding: 24,
        paddingBottom: 280
    },
    headerText: {
        fontSize: 25,
        fontFamily: 'Roboto-Medium',
        marginTop: 28
    },
    wrapperVerticalScroll: {
        marginTop: 18
    },
    ruler: {
        borderWidth: 1.5,
        marginTop: 20,
        borderColor: 'grey'
    },
    price: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginTop: 10
    },
    totalText: {
        fontFamily: 'Roboto-Bold',
        fontSize: 18
    },
    priceNumber: {
        color: '#FFA800',
        fontSize: 20,
        fontFamily: 'Roboto-Bold'
    },
    buttonContainer: {
        alignItems: 'center'
    }
})
