import React, { useEffect } from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'
import { Logo, MadeBy } from '../../assets'

export default function SplashScreen({navigation}) {
    useEffect(() => {
        setTimeout(() => {
            navigation.replace('Login')
        }, 3000)
    }, [navigation])

    return (
        <View style={styles.screen}>
            <View style={styles.logoContainer}>
                <Logo />
            </View>
            <MadeBy />
        </View>
    )
}

const styles = StyleSheet.create({
    screen: {
        alignItems: 'center',
        justifyContent: 'flex-end',
        flex: 1
    },
    logoContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})
