import React from 'react'
import { StyleSheet, Text, View, TextInput, TouchableOpacity, ScrollView } from 'react-native'
import { Logo } from '../../assets'
import { ButtonCustom } from '../../components'

export default function Register({navigation}) {
    return (
        <View style={styles.page}>
            <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={styles.content}>
                <Logo />
                <View>
                    <Text style={styles.label}>Username</Text>
                    <TextInput style={styles.textInput} />
                    <Text style={styles.label}>Alamat</Text>
                    <TextInput placeholder="Jln." style={styles.textInput} />
                    <Text style={styles.label}>Email</Text>
                    <TextInput placeholder="user@gmail.com" keyboardType="email-address" style={styles.textInput} />
                    <Text style={styles.label}>Password</Text>
                    <TextInput secureTextEntry style={styles.textInput} />
                </View>
                <ButtonCustom title="Daftar" onPress={() => {}} />
                <TouchableOpacity onPress={() => navigation.navigate('Login')}>
                    <Text style={{ marginTop: 20, color: '#FFA800' }}>Sudah Memiliki Akun? Login</Text>
                </TouchableOpacity>
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    page: {
        flex: 1,
    },
    content: {
        alignItems: 'center',
        justifyContent: 'center',
        height: '100%'
    },
    label: {
        fontFamily: 'Roboto',
        marginBottom: 2,
        marginTop: 15
    },
    textInput: {
        width: 252,
        height: 60,
        borderWidth: 2,
        borderColor: '#FFA800',
        borderRadius: 8,
        fontSize: 20,
        padding: 10
    },
    button: {
        width: 232,
        height: 55,
        borderRadius: 6,
        backgroundColor: '#FFA800',
        marginTop: 26,
        alignItems: 'center',
        justifyContent: 'center',
    },
})
