import {createStore} from 'redux'

const initialState = {
    orders: [],
    checkout: [],
    disable: false
}

const reducer = (state = initialState, action) => {
    switch(action.type){
        case 'ORDER' :
            let order = {
                food: action.value.food,
                price: action.value.price,
                image: action.value.image,
            }
            return {
                ...state,
                orders: state.orders.concat(order),
            }
        case 'CANCEL' : 
            return {
                orders: [],
                disable: false
            }
    }
    return state;
}

const store = createStore(reducer);
export default store;
